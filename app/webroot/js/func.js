var $debug = true;

jQuery.fn.defuscate = function(settings) {
	settings = jQuery.extend({link: true}, settings);
	regex = /\b([A-Z0-9._%-]+)\([^)]+\)((?:[A-Z0-9-]+\.)+[A-Z]{2,6})\b/gi;
classes = $(this).attr("class");
mailto = '<a href="mailto:$1@$2" class="'+classes+'">$1@$2</a>';
plain = "$1@$2";
return this.each(function() {
	defuscated = $(this).html().replace(regex, settings.link ? mailto : plain)
  $(this).after(defuscated).remove();
});
};

$(window).load(function(){
  if($('.blueberry').length > 0) {
    $('.blueberry').blueberry({
      'crop': false
    });  
  }
  
});

$(document).ready(function(){
  if($debug) { $('body').addClass('debug'); }
  $('html').removeClass('no-js').addClass('js');
  // $('#navigation-main ul').mobileMenu({switchWidth:680});
  $('#navigation-main li ul').each(function(){
  	$(this).parents('li').addClass('has-sub');
  });


  cookieResize('#textresize a', 'medium');  

  if($('#sidebar #filter').length > 0) {
    $('#sidebar #filter label').click(function(){
      if($(this).find('input').is(':checked')) {
        $(this).addClass('active');
      }
      else {
        $(this).removeClass('active');
      }
    })
  }

  if($('#searchform').length > 0) {
    $('#search').before('<div id="toggle" class="clearfix col span_6"></div>');

    $('#searchform .searchbox:not(:first)').hide();
    $('#searchform .searchbox').each(function(){
      target = $(this).attr('data-target');
      label = $(this).find('h1').text();
      $('#toggle').append('<span class="toggle '+target+'" data-target="'+target+'">'+label+'</span>');
      $(this).find('h1').remove();
    });

    $('#toggle .toggle:first').addClass('active');

    $('#toggle .toggle').click(function(){
      $(this).parent().find('.toggle').removeClass('active');
      $(this).addClass('active');
      target = $(this).attr('data-target');
      $('.searchbox').hide();
      $('.searchbox.'+target).show();
    })
  }

});

