<?php
/**
 * PetFixture
 *
 */
class PetFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'pet_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'Tierart' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Rasse' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Farbe' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'groesse_cm' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 5),
		'geb_jahr' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 5),
		'merkmale' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'freitext' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1000, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'pet_id' => 1,
			'Tierart' => 'Lorem ipsum dolor ',
			'Rasse' => 'Lorem ipsum dolor ',
			'Farbe' => 'Lorem ipsum dolor ',
			'groesse_cm' => 1,
			'geb_jahr' => 1,
			'merkmale' => 'Lorem ipsum dolor sit amet',
			'freitext' => 'Lorem ipsum dolor sit amet'
		),
	);

}
