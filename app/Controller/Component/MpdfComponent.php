<?php
App::import ('Vendor', 'mpdf', array('file' => 'pdf',
    'mpdf' . DS . 'mpdf.php'));

@set_time_limit(10000);

/**
 * M-PDF Componente
 * Erstellt PDF-Dateien aus übergeben Inhalten
 *
 * Debug-Parameter:
 * pdfdebug = 1 -> Keine Weiterleitung auf das gespeicherte
Dokument
 * pdfsource = 1 -> Ausgabe der PDF Quelle
 */

class MpdfComponent extends Component
{
    public $stylesheet;


    /**
     * Controller holen
     */
    function initialize(&$controller, $settings = array())
    {
        $this->controller = $controller;

    }

		/**
         * Funktion zur Ausgabe der PDF-Datei
         * var $path_to_pdf = Path to Pdf-file
         * var $dest = I: send the file inline to the browser.
        The plug-in is used if available. The name given by filename is used
        when one selects the "Save as" option on the link generating the PDF.
        D: send to the browser and force a file download
        with the name given by filename.
        F: save to a local file with the name given by
        filename (may include a path).
        S: return the document as a string. filename is
        ignored.
         *
         * @author Stephan
         */

    function create_pdf($html, $pdfname = '',$path_to_pdf =
    false, $dest = 'I') {

        // Quelle ausgeben
        if(!empty($this->controller->params['named']['pdfsource']))
        {
            echo $html;
            exit;
        }


        // Fehlermeldungen ausblenden, da MPdf einige Fehler mitsich bringt

			$temp_debug_mode = Configure::read('debug');

			Configure::write('debug',null);


			// Neues Dokument beginnen
               include('C:\xampp\htdocs\findmypet\fmp\app\Vendor\mpdf\mpdf.php');

            $mpdf = new mPDF();



			// DPI-Wert anpassen @TODO Anpassungen
			$mpdf->dpi = 600;
        //$stylesheet = $this->stylesheet;
            $stylesheet = file_get_contents('');
            //$stylesheet = ;
//			if (!empty($this->stylesheet)) {
//                $stylesheet = $this->stylesheet;
//            } else {
//                // CSS-Style holen @TODO Anpassungen
//                $stylesheet =
//                    file_get_contents(Url::_('/dyncss/css.php?l='.BMS_PORTAL.'&t=apps&x=extension_pdf&o=extension'));
//            }

        // PDF-Datei vorbereiten (Nur Styles) Siehe:http://mpdf1.com/manual/index.php?tid=121&searchstring=writehtml
        $mpdf->writeHTML ($stylesheet, 0);

        // PDF-Datei HTML-Inhalt schreiben
        $mpdf->writeHTML ($html, 2);

        // Inline Send Debug
        if(!empty($this->controller->params['named']['pdfdebug']))
        {
            $dest = 'I';
        }

        // Soll PDF-Datei lokal gespeichert werden? ->Pfadnamen anhängen
			if($dest == 'F' && !empty($path_to_pdf)) {
                $pdfname = $path_to_pdf.$pdfname;

                // PDF-Datei ausgeben
                $mpdf->Output ($pdfname, $dest);

            } else {
                // PDF-Datei ausgeben
                $mpdf->Output ($pdfname, $dest);
                exit;
            }

			Configure::write('debug',$temp_debug_mode);
		}
}