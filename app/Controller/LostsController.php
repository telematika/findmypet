<?php
App::uses('AppController', 'Controller');

/**
 * Losts Controller
 *
 * @property Lost $Lost
 * @property PaginatorComponent $Paginator
 */
class LostsController extends AppController {

    public $helpers = array('Html'); //Jquery Helper einbinden
    public $components = array('Mpdf','RequestHandler');
    public $layout = 'default';

    /**
     * @param $pet
     */
    public function index() {

        $this->Lost->recursive = 0;
        $options = array('fields' => array('Pet.name'),
                         'conditions' => array('user_id' => $this->Session->read('Auth.User.id')
                         ));
        $optionsAll = array('fields' => array('Pet.name', 'Pet.Farbe', 'Pet.groesse_cm', 'Pet.merkmale', 'Pet.Rasse'),
                            'conditions' => array('user_id' => $this->Session->read('Auth.User.id')),
                            'options' => array('Pet.id' => 'Pet.name'));
        $userpetsAll = $this->Lost->Pet->find('all', $optionsAll);
        $userpets = $this->Lost->Pet->find('list', $options);
        //var_dump($userpets);
        $this->set('losts', $userpets);
        $this->set('lostsAll', $userpetsAll);

        if (!empty($this->request->data)) {
            if ($this->request->is('post')) {
                $this->Lost->create();
            if ($this->Lost->save($this->request->data)){

//                if ($this->request->is('ajax')){
//                    $this->render('#drop', 'ajax');
//                }
//                else {
//                    $test = $this->request->data;
//                    var_dump($test);
                    $this->Session->setFlash('Lost report has been saved');
                    $this->redirect(array('action'=>'index'));
                }
            }
        }
    }

    public function add() {
//        $url = 'http://localhost/findmypet/fmp/losts/add';
//        $html = file_get_contents($url);
//        $lost = 'Lost';
//        $filename = 'test';
//        $desti = 'I';
//        header_remove();
//        $this->Mpdf->initialize($lost);
//        $this->set('data',$this->Mpdf->create_pdf($html));
//

        $this->Lost->recursive = 0;
        $options = array('fields' => array('Pet.name'),
            'conditions' => array('user_id' => $this->Session->read('Auth.User.id'),
            ));
        $userpets = $this->Lost->Pet->find('list', $options);
        $this->set('losts', $userpets);

        if ($this->request->is('post')) {
            $petid = $this->request->data['Lost']['Tiere'];
            return $this->redirect(
                array('action' => 'view', 'id' => $petid));

        }
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {

        extract($this->request->params['named']);
        if (!$this->Lost->Pet->exists($id)) {
            throw new NotFoundException(__('Invalid pet'));
        }
        $this->Lost->Pet->id = $id;
        $options = array('conditions' =>array('Pet.' .$this->Lost->Pet->primaryKey => $id),
               array('user_id' => $this->Session->read('Auth.User.id')));
        $this->set('petsAll', $this->Lost->Pet->find('first', $options));

        if ($this->request->is('post')) {
            $this->Lost->save($this->request->data);
            var_dump($this->request->data);
            return $this->redirect(
                array('action' => 'view_pdf', 'id' => $id));

        }

    }
    function view_pdf($id = null)
    {
        extract($this->request->params['named']);
        if (!$this->Lost->Pet->exists($id)) {
            throw new NotFoundException(__('Invalid pet'));
        }
        $this->Lost->Pet->id = $id;
        $options = array('conditions' =>array('Pet.' .$this->Lost->Pet->primaryKey => $id),
            array('user_id' => $this->Session->read('Auth.User.id')));
        $this->set('petsAll', $this->Lost->Pet->find('first', $options));



            $this->layout = 'pdf';
            $view = new View($this, false);
            $view->viewPath = 'losts';
            $html = $view->render('view_pdf');
            $lost = 'Lost';
            header_remove();
            $this->Mpdf->initialize($lost);
            $this->set('data',$this->Mpdf->create_pdf($html));


    }
}