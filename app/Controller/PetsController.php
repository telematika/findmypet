<?php
App::uses('AppController', 'Controller');
/**
 * Pets Controller
 *
 * @property Pet $Pet
 * @property PaginatorComponent $Paginator
 */
class PetsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
    public function login(){
        if ($this->request->is('post')){
            if ($this->Auth->login()){
                //return $this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'home');
                return $this->redirect($this->Auth->redirect());
            }
            $this->Session->setFlash(_('inkorrekter Benutzername oder Passwort, bitte erneut versuchen'));
        }
    }
    public function logout(){
        return $this->redirect($this->Auth->logout());
    }


//list all pets of the authenticated user
	public function index() {
        $this->Pet->recursive = 0;
        $this->set('pets', $this->Paginator->paginate('Pet', array('user_id' => $this->Session->read('Auth.User.id'))));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
        extract($this->request->params['named']);
        $this->uploadPicture();

		if (!$this->Pet->exists($id)) {
			throw new NotFoundException(__('Invalid pet'));
		}
        $this->Pet->id = $id;
		$options = array('conditions' => array('Pet.' . $this->Pet->primaryKey => $id),
                   array('user_id' => $this->Session->read('Auth.User.id')));
                   ;
		$this->set('pet', $this->Pet->find('first', $options));
	}

    private function uploadPicture($id = null){
       // var_dump($this->request->data['Pet']);
        if(!empty($this->request->data['PetPic']['bild']['tmp_name'])){
            //var_dump($this->request->data['Pet']);
            if(move_uploaded_file($this->request->data['PetPic']['bild']['tmp_name'],
                'C:\xampp\htdocs\findmypet\fmp\app\webroot\img\\'.$this->request->data['PetPic']['bild']['name'])){
                $this->Pet->id= $id;#id zur Speicherung an die richige Stelle
                //var_dump($id);
                $data = array(
                        'id' => $id,
                        'bild' =>$this->request->data['PetPic']['bild']['name']);
                //$this->Pet->save($data);
               }
        }
        //return 'C:\xampp\htdocs\findmypet\fmp\app\webroot\img\\'.$this->request->data['Pet']['bild']['name'];
    }
/**
 * add method
 *
 * @return void
 */
	public function add() {
        $this->uploadPicture($id = null);
		if ($this->request->is('post')) {
                        $this->Pet->create();

			if ($this->Pet->save($this->request->data)) {
				$this->Session->setFlash(__('The pet has been saved.'));

			} else {
				$this->Session->setFlash(__('The pet could not be saved. Please, try again.'));
			}

		}
	}

    private function savePic($id = null){
        $this->Pet->id= $id;#id zur Speicherung an die richige Stelle
        //var_dump($id);
        if ($this->request->is(array('post', 'put'))) {
            var_dump($this->request->data);
            //$this->uploadPicture($id);

        $data = array(
            'id' => $id,
            'bild' =>$this->request->data['PetPic']['bild']['name']);
        $this->Pet->save($data);

        }
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        //var_dump($this->request->data['Pet']);
        extract($this->request->params['named']);
        if (!$this->Pet->exists($id)) {
			throw new NotFoundException(__('Invalid pet'));
		}
        $this->Pet->id = $id;
        $optionsPet = array('conditions' => array('Pet.' . $this->Pet->primaryKey => $id),
            array('user_id' => $this->Session->read('Auth.User.id')));
        $this->set('pet', $this->Pet->find('first', $optionsPet));



        if ($this->request->is(array('post', 'put'))) {
            //prüfen welcher Button angeclickt wurde
            if(isset($this->request->data['PetPic']['bild']['name'])){
                $this->Pet->id = $id;
                $optionsPet = array('conditions' => array('Pet.' . $this->Pet->primaryKey => $id),
                    array('user_id' => $this->Session->read('Auth.User.id')));
                $this->set('pet', $this->Pet->find('first', $optionsPet));

                var_dump($this->request->data);
                $this->uploadPicture($id);
                $this->savePic($id);
                $this->Session->setFlash(__('Das Bild wurde hochgeladen.'));
                return $this->redirect(array(
                    'action' => 'edit',
                    'id'=> $this->Pet->id
                ));


            }
            elseif(isset($this->request->data['Pet'])){
                $this->Pet->id = $id;
                $optionsPet = array('conditions' => array('Pet.' . $this->Pet->primaryKey => $id),
                    array('user_id' => $this->Session->read('Auth.User.id')));
                $this->set('pet', $this->Pet->find('first', $optionsPet));

                var_dump($this->request->data);

            if ($this->Pet->save($this->request->data['Pet'])) {
  				$this->Session->setFlash(__('The pet has been saved.'));

  				return $this->redirect(array(
                    'action' => 'view',
                    'id' => $this->Pet->id

                ));
  			}
            else {
  				$this->Session->setFlash(__('The pet could not be saved. Please, try again.'));
  			}
		}
        }

  			$options = array('conditions' => array('Pet.' . $this->Pet->primaryKey => $id));
  			$this->request->data = $this->Pet->find('first', $options);


	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Pet->id = $id;
		if (!$this->Pet->exists()) {
			throw new NotFoundException(__('Invalid pet'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Pet->delete()) {
			$this->Session->setFlash(__('The pet has been deleted.'));
		} else {
			$this->Session->setFlash(__('The pet could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function petpicture($id = null){
        extract($this->request->params['named']);
        $this->uploadPicture($id = null);


        $this->Pet->id = $id;
        $pet = $this->Pet->id;
        $this->set('pet', $pet);

//        $options = array('conditions' =>array('Pet.' .$this->Pet->primaryKey => $id),
//            array('user_id' => $this->Session->read('Auth.User.id')));
//        $this->set('pet', $this->Pet->find('first', $options));


    }

}