<?php
App::uses('AppController', 'Controller');
/**
 * Created by PhpStorm.
 * User: Konrad
 * Date: 01.04.14
 * Time: 11:33
 */
class FoundsController extends AppController {

    public $components = array('Paginator');
    # public $helpers = array('Html', 'Form');
    public $helpers = array('GoogleMap');
    public function index() {

        $this->Found->recursive = 0;
        $this->set('founds', $this->Paginator->paginate());
        $this->set('title_for_layout', 'Find My Pet Index');
    }

    public function view($id = null) {
        if (!$this->Found->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('Found.' . $this->Found->primaryKey => $id));
        $this->set('found', $this->Found->find('first', $options));
    }
    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Found->create();

            if ($this->Found->save($this->request->data)) {
                $this->Session->setFlash(__('The pet has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The pet could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Found->exists($id)) {
            throw new NotFoundException(__('Invalid pet'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Found->save($this->request->data)) {
                $this->Session->setFlash(__('The pet has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The pet could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Found.' . $this->Found->primaryKey => $id));
            $this->request->data = $this->Found->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Found->id = $id;
        if (!$this->Found->exists()) {
            throw new NotFoundException(__('Invalid pet'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Found->delete()) {
            $this->Session->setFlash(__('The pet has been deleted.'));
        } else {
            $this->Session->setFlash(__('The pet could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }


}


