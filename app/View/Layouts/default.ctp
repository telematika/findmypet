<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Testseite_FMP');
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->css('https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css'); ?>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $title_for_layout; ?>
    </title>

    <?php
    echo $this->Html->meta('icon');

    echo $this->Html->css('cake.generic');
    //echo $this->Html->css('styles');
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    echo $this->Js->writeBuffer(array('cache'=>TRUE)); //Ausgabe der gecacheden Skripts

    ?>
</head>
<body>

<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
</style>

<table class="tg">
    <tr>
        <th class="tg-031e" colspan="5">
            <div id="container">
                <div id="header">
                    <div style="text-align:right">
                        <?php
                        if (AuthComponent::user()) {
                            echo $this->Html->link(__('Logout'), array('action' => 'logout' ));
                            echo 'angemeldet: ';
                            print $this->Session->read('Auth.User.username');
                        }
                        else{
                            echo $this->Html->link(__('Login'), array('action' => 'login' ));
                        }
                        ?>
                    </div>
                    <div style="text-align:left">

                        <?php echo $this->Html->link(__('Startseite'), array('controller' => 'users', 'action' => 'index')); ?>
                        <?php echo $this->Html->link(__('Tiere'), array('controller' => 'pets', 'action' => 'index')); ?>
                    </div>
        </th>
    </tr>
    <tr>
        <td class="tg-031e" colspan="5">
            <div align="center"> <h3> <?php echo 'Willkommen bei Find my Pet' ?> </h3></div>

        </td>
    </tr>
    <tr>
        <td class="tg-031e" rowspan="2">

        </td>
        <td class="tg-031e" colspan="3">
            <h1>
                <!-- <?php echo $this->Html->link($cakeDescription, array('action'=>'index')); ?></h1> -->
                </div>
                <div id="content">
                    <?php //echo $content_for_layout; ?>
                    <?php echo $this->Session->flash(); ?>

                    <?php echo $this->fetch('content'); ?>
                </div>

                </div>
        </td>
        <td class="tg-031e" rowspan="2"></td>
    </tr>
    <tr>
        <td class="tg-031e" colspan="3">
            <div id="footer">
                <?php echo $this->Html->link(
                    $this->Html->image('Logo_mit_Balken.gif', array('alt' => $cakeDescription, 'border' => '0')),
                    'http://www.telematika.de/',
                    array('target' => '_blank', 'escape' => false)
                );
                ?>
            </div>
        </td>
    </tr>
</table>

</body>
</html>
