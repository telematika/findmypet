<div class="users form">
    <?php echo $this->Session->flash('auth');?>
    <?php echo $this->Form->create('User');?>
    <fieldset>
        <legend>
            <?php echo __('Bitte geben Sie Ihren Benutzernamen und Ihr Passwort ein')?>
        </legend>
        <?php echo $this->Form->input('username');
        echo $this->Form->input('password');
        ?>

    </fieldset>
    <?php echo $this->Form->end(__('Login'));?>
</div>

<div class="actions">
    <h1><?php echo __('Neuen Benutzer anlegen'); ?></h1>
    <ul>
        <li><?php echo $this->Html->link(__('Neuer Bentzer'), array('action' => 'add')); ?></li>
    </ul>
</div>



