<div class="users form">
    <?php echo $this->Session->flash('auth'); ?>
    <?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend>
            <?php echo __('Bitte geben Sie Ihren Benutzernamen und Ihr Passwort ein'); ?>
        </legend>
        <?php echo $this->Form->input('username');
        echo $this->Form->input('passw0rd');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Login')); ?>
</div>