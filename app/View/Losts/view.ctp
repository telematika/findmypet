<head> </head>
<body>
<h2><?php echo __('Detailansicht lost pet'); ?></h2>

<?php $this->start('details'); ?>
<div class="details" id="details">
    <h2><?php echo __('Lost'); ?></h2>
    <dl>
        <dt><?php echo __('Name'); ?></dt>
        <dd>
            <?php echo h($petsAll['Pet']['name']); ?>
            &nbsp;
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($petsAll['Pet']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Tierart'); ?></dt>
        <dd>
            <?php echo h($petsAll['Pet']['Tierart']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Rasse'); ?></dt>
        <dd>
            <?php echo h($petsAll['Pet']['Rasse']); ?>
            &nbsp
        </dd>
        <dt><?php echo __('Farbe'); ?></dt>
        <dd>
            <?php echo h($petsAll['Pet']['Farbe']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Groesse Cm'); ?></dt>
        <dd>
            <?php echo h($petsAll['Pet']['groesse_cm']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Geburtsjahr'); ?></dt>
        <dd>
            <?php echo h($petsAll['Pet']['geb_jahr']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Merkmale'); ?></dt>
        <dd>
            <?php echo h($petsAll['Pet']['merkmale']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Suchmeldung'); ?></dt>
        <dd>

            <?php echo $this->Form->create('Lost');?>
            <?php echo $this->Form->input('freitext',array(
                'type' => 'textarea'
            )); ?>
            <?php echo $this->Form->hidden('pet_id', array('value'=> $petsAll['Pet']['id']));?>

            <?php echo $this->Form->end('Vermisstenanzeige aufgeben'); ?>
</div>
<?php $this->end();?>
<div>
<?php echo $this->fetch('details'); ?>
</div>
</body>