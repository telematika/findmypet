<!--/*App::import('Vendor','xtcpdf');
$tcpdf = new XTCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans' 

$tcpdf->SetAuthor("Telematika GmbH");
$tcpdf->SetAutoPageBreak( false );
$tcpdf->setHeaderFont(array($textfont,'',40));
$tcpdf->xheadercolor = array(150,0,0);
$tcpdf->xheadertext = 'FindMyPet';
$tcpdf->xfootertext = 'Copyright FindMyPet. All rights reserved.';

// add a page (required with recent versions of tcpdf) 
$tcpdf->AddPage();

// Now you position and print your page content 
// example:  
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,14, "Hello World", 0,1,'L');
// ... 
// etc. 
// see the TCPDF examples  

echo $tcpdf->Output('filename.pdf', 'D');*/-->

<head> </head>
<body>
<h2><?php echo __('Vermisstenanzeige'); ?></h2>

<?php $this->start('details'); ?>
<div class="details" id="details">

        <?php echo __('Name: '); ?>
        <?php echo h($petsAll['Pet']['name']); ?>
        <p>
        <?php echo __('Id:'); ?>
        <?php echo h($petsAll['Pet']['id']); ?>
        </p>
        <p>
        <?php echo __('Tierart: '); ?>
        <?php echo h($petsAll['Pet']['Tierart']); ?>
        </p>
        <p>
        <?php echo __('Rasse: '); ?>
        <?php echo h($petsAll['Pet']['Rasse']); ?>
        </p>
        <P>
        <?php echo __('Farbe: '); ?>
        <?php echo h($petsAll['Pet']['Farbe']); ?>
        </P>
        <p>
        <?php echo __('Groesse Cm: '); ?>
        <?php echo h($petsAll['Pet']['groesse_cm']); ?>
        </p>
        <p>
        <?php echo __('Geburtsjahr: '); ?>
        <?php echo h($petsAll['Pet']['geb_jahr']); ?>
        </p>
        <p>
        <?php echo __('Merkmale: '); ?>
        <?php echo h($petsAll['Pet']['merkmale']); ?>
        </p>
        <?php   if(isset($petsAll['Lost']['freitext'])){
        echo __('Freitext: '); ?>
        <?php echo h($petsAll['Lost']['freitext']); }?>
        <p>
            <?php echo __('Bild: '); ?>
            <?php echo $this->Html->image($petsAll['Pet']['bild']); ?>
        </p>

</div>
<?php $this->end();?>
<div>
    <?php echo $this->fetch('details'); ?>
</div>
</body>

