<div id ='drop' class="losts form">
    <?php echo $this->Html->script('jquery-2.1.0'); ?>

    <h2><?php echo __('Losts'); ?></h2>
    <!--Drop Down Menu für Tiere des angemeldeteten Nutzers erstellen-->
    <?php //var_dump($losts) ?>
    <?php $this->Form->create('Pet', array('type' => 'post'));
        echo $this->Form->input('Pet.name', array(
            'type' => 'select',
            'options' => array($losts),
            'empty' => array('Tier auswählen')
            )); ?>

    <?php echo $this->Form->end(__('Tier auswählen')); ?>

    <div id="sending" style="display: none; background-color: lightgreen;"> Sending...
    <p>
        <?php echo $json ?>
    </p>
    </div>

    <p></p><?php echo __('Ist Dein Tier noch nicht registriert?'); ?><p></p>
    <!--Neues Tier anlegen, für den Fall das Nutzer noch keins angelegt hat-->
    <ul>
        <li><?php echo $this->Html->link(__('Neues Tier anlegen'), array('controller'=> 'pets','action' => 'add')); ?></li>
    </ul>
</div>
</div>