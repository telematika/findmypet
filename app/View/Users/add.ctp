<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Add User'); ?></legend>
	<?php
		echo $this->Form->input('vorname');
		echo $this->Form->input('nachname');
		echo $this->Form->input('strasse');
		echo $this->Form->input('plz');
		echo $this->Form->input('stadt');
		echo $this->Form->input('pw');
		echo $this->Form->input('newsletter');
		echo $this->Form->input('kk');
		echo $this->Form->input('tel');
		echo $this->Form->input('email');
                echo $this->Form->input('username');
                echo $this->Form->input('password');


		if (AuthComponent::user()) {
            echo $this->Form->input('role', array(
                'options' => array('admin' => 'Admin', 'author'=>'Author')
            ));
        }



	?>
	</fieldset>
<?php echo $this->Form->end(__('Abschicken')); ?>
</div>
<!--
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
	</ul>
</div>
-->
