<div class="users index">
	<h2><?php echo __('Users'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('vorname'); ?></th>
			<th><?php echo $this->Paginator->sort('nachname'); ?></th>
			<th><?php echo $this->Paginator->sort('strasse'); ?></th>
			<th><?php echo $this->Paginator->sort('plz'); ?></th>
			<th><?php echo $this->Paginator->sort('stadt'); ?></th>
			<th><?php echo $this->Paginator->sort('pw'); ?></th>
			<th><?php echo $this->Paginator->sort('newsletter'); ?></th>
			<th><?php echo $this->Paginator->sort('kk'); ?></th>
			<th><?php echo $this->Paginator->sort('tel'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['vorname']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['nachname']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['strasse']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['plz']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['stadt']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['pw']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['newsletter']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['kk']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['tel']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['modified']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['created']); ?>&nbsp;</td>
		<td class="actions">

            <?php
            if ($this->Session->read('Auth.User.id') == $user['User']['id']) {
                echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id']));
                echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id']));
                echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), null, __('Sind Sie sicher das Sie den Nutzer %s löschen wollen?', $user['User']['nachname']));
                        }
            else{
            }
            ?>

		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Neuer User'), array('action' => 'add')); ?></li>
	</ul>
</div>
