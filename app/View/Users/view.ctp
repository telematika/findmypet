<div class="users view">
<h2><?php echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vorname'); ?></dt>
		<dd>
			<?php echo h($user['User']['vorname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nachname'); ?></dt>
		<dd>
			<?php echo h($user['User']['nachname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Strasse'); ?></dt>
		<dd>
			<?php echo h($user['User']['strasse']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Plz'); ?></dt>
		<dd>
			<?php echo h($user['User']['plz']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Stadt'); ?></dt>
		<dd>
			<?php echo h($user['User']['stadt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pw'); ?></dt>
		<dd>
			<?php echo h($user['User']['pw']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Newsletter'); ?></dt>
		<dd>
			<?php echo h($user['User']['newsletter']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Kk'); ?></dt>
		<dd>
			<?php echo h($user['User']['kk']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tel'); ?></dt>
		<dd>
			<?php echo h($user['User']['tel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), null, __('Sind Sie sicher das Sie den Nutzer %s löschen wollen?', $user['User']['nachname'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Neuer User'), array('action' => 'add')); ?> </li>
	</ul>
</div>
