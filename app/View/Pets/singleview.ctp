<div class="pets index">
    <h2><?php echo __('Pets'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('Tierart'); ?></th>
            <th><?php echo $this->Paginator->sort('Rasse'); ?></th>
            <th><?php echo $this->Paginator->sort('Farbe'); ?></th>
            <th><?php echo $this->Paginator->sort('groesse_cm'); ?></th>
            <th><?php echo $this->Paginator->sort('geb_jahr'); ?></th>
            <th><?php echo $this->Paginator->sort('merkmale'); ?></th>
            <th><?php echo $this->Paginator->sort('freitext'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        <?php foreach ($pets as $pet): ?>
            <tr>

                <td><?php echo h($pet['Pet']['id']); ?>&nbsp;</td>
                <td><?php echo h($pet['Pet']['Tierart']); ?>&nbsp;</td>
                <td><?php echo h($pet['Pet']['Rasse']); ?>&nbsp;</td>
                <td><?php echo h($pet['Pet']['Farbe']); ?>&nbsp;</td>
                <td><?php echo h($pet['Pet']['groesse_cm']); ?>&nbsp;</td>
                <td><?php echo h($pet['Pet']['geb_jahr']); ?>&nbsp;</td>
                <td><?php echo h($pet['Pet']['merkmale']); ?>&nbsp;</td>
                <td><?php echo h($pet['Pet']['freitext']); ?>&nbsp;</td>
                <td class="actions">
                    <?php
                    if ($this->Session->read('Auth.User.id') == $pet['Pet']['user_id']){
                        echo $this->Html->link(__('View'), array('action' => 'view', $pet['Pet']['id']));
                        echo $this->Html->link(__('Edit'), array('action' => 'edit', $pet['Pet']['id']));
                        echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $pet['Pet']['id']), null, __('Are you sure you want to delete # %s?', $pet['Pet']['id']));
                    }
                    else{
                    }
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('New Pet'), array('action' => 'add')); ?></li>
    </ul>
</div>
