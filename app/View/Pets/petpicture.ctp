<div class="petpicture form">
    <?php echo $this->Form->create('Picture',array('type'=>'file','controller'=>'pets',
                                   'action'=>'add', 'url'=>'/pets/petpicture/'.$pet['Pet']['id'])); ?>
    <fieldset>
        <legend><?php echo __('Bild auswählen'); ?></legend>

<?php
        echo $this->Form->input('file', array('type'=>'file'));
        echo $this->Form->end(__('Hochladen'));

        if (isset($pic)) {
            echo $this->Html->image($pic);
        }
?>
    </fieldset>