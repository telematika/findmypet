<div class="pets view">
<h2><?php echo __('Pet'); ?></h2>
	<dl>
        <dt><?php echo __('Name'); ?></dt>
        <dd>
            <?php echo h($pet['Pet']['name']); ?>
            &nbsp;
        </dd>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($pet['Pet']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tierart'); ?></dt>
		<dd>
			<?php echo h($pet['Pet']['Tierart']); ?>
            &nbsp;
		</dd>
		<dt><?php echo __('Rasse'); ?></dt>
		<dd>
			<?php echo h($pet['Pet']['Rasse']); ?>
			&nbsp
		</dd>
		<dt><?php echo __('Farbe'); ?></dt>
		<dd>
			<?php echo h($pet['Pet']['Farbe']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Groesse Cm'); ?></dt>
		<dd>
			<?php echo h($pet['Pet']['groesse_cm']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Geburtsjahr'); ?></dt>
		<dd>
			<?php echo h($pet['Pet']['geb_jahr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Merkmale'); ?></dt>
		<dd>
			<?php echo h($pet['Pet']['merkmale']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Freitext'); ?></dt>
		<dd>
			<?php echo h($pet['Pet']['freitext']); ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Bild');?></dt>
        <dd>
            <?php echo h($pet['Pet']['bild']);
            if(isset($pet)){
                echo $this->Html->image($pet['Pet']['bild']);
            }?>
            &nbsp;
        </dd>

	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Pet'), array('action' => 'edit', $pet['Pet']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Pet'), array('action' => 'delete', $pet['Pet']['id']), null, __('Are you sure you want to delete # %s?', $pet['Pet']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Pets'), array('action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List my Pets'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pet'), array('action' => 'add')); ?> </li>
	</ul>
</div>
