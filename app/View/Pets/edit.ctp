<div class="pets form">
    <?php
    if(isset($pet)){
        echo $this->Html->image($pet['Pet']['bild']);
    } ?>
<?php echo $this->Form->create('PetPic', array('type' => 'file', 'action' => 'edit', 'url'=>'/pets/edit/'.$pet['Pet']['id'])); ?>
   <?php echo $this->Form->input('PetPic.bild', array(
    'type' => 'file',
    'label' => false
    )); ?>
    <?php echo $this->Form->end(__('Bild hochladen')); ?>
</div>
<div class="pets form">
<?php echo $this->Form->create('Pet', array('action' => 'edit', 'url'=>'/pets/edit/'.$pet['Pet']['id'])); ?>
	<fieldset>
		<legend><?php echo __('Edit Pet'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('Tierart');
		echo $this->Form->input('Rasse');
		echo $this->Form->input('Farbe');
		echo $this->Form->input('groesse_cm');
		echo $this->Form->input('geb_jahr');
		echo $this->Form->input('merkmale');
		echo $this->Form->input('freitext');


    //var_dump($_FILES);
	?>
<!--        --><?php //echo $this->Form->create('Picture',array('type'=>'file', 'action'=>'add' ,'url'=>'/pets/edit/'.$pet['Pet']['id']));
//        echo $this->Form->input('file', array('type'=>'file'));
//        echo $this->Form->end('Hochladen');

        ?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>

</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Pet.pet_id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Pet.pet_id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Pets'), array('action' => 'index')); ?></li>
	</ul>
</div>
