<div class="pets form">
<?php echo $this->Form->create('Pet'); ?>
	<fieldset>
		<legend><?php echo __('Add Pet'); ?></legend>

    <?php
        echo $this->Form->input('name');
		echo $this->Form->input('Tierart');
		echo $this->Form->input('Rasse');
		echo $this->Form->input('Farbe');
		echo $this->Form->input('groesse_cm');
		echo $this->Form->input('geb_jahr');
		echo $this->Form->input('merkmale');
		echo $this->Form->input('freitext');

        echo $this->Form->hidden('user_id', array('value'=>$this->Session->read('Auth.User.id')));



?>
	</fieldset>
<?php echo $this->Form->end('Weiter'); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('List Pets'), array('action' => 'index')); ?></li>
	</ul>
</div>
