<?php $this->Html->script("//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js", false); ?>
<?php $this->Html->script("http://maps.google.com/maps/api/js?sensor=false", false); ?>
<?php
// Override any of the following default options to customize your map
$map_options = array(
    'id' => 'map_canvas',
    'width' => '950px',
    'height' => '300px',
    'style' => '',
    'zoom' => 18,
    'type' => 'HYBRID',
    'custom' => null,
    'localize' => true,
    'latitude' => 40.69847032728747,
    'longitude' => -1.9514422416687,
    'address' => '1 Infinite Loop, Cupertino',
    'marker' => true,
    'markerTitle' => 'This is my position',
    'markerIcon' => 'http://www.google.com/mapfiles/markerA.png',
    'markerShadow' => 'http://google-maps-icons.googlecode.com/files/shadow.png',
    'infoWindow' => true,
    'windowText' => 'My Position'
);
?>
<div class="pets form">

    <?php echo $this->Form->create('Pet'); ?>

    <fieldset>
        <legend><?php echo __('Add Pet'); ?></legend>
        <?php
        echo $this->Form->input('name');
        echo $this->Form->input('Tierart');
        echo $this->Form->input('Rasse');
        echo $this->Form->input('Farbe');
        echo $this->Form->input('groesse_cm');
        echo $this->Form->input('merkmale');
        echo $this->Form->input('freitext');
        echo $this->Form->input('adresse');
        echo $this->GoogleMap->map($map_options);
        echo $this->Form->hidden('user_id', array('value'=>$this->Session->read('Auth.User.id')));

        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>


<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Upload Picture'), array('action' => 'uploadPicture')); ?></li>
        <li><?php echo $this->Html->link(__('List Pets'), array('action' => 'index')); ?></li>
    </ul>
</div>
