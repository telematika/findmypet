<div class="founds view">
    <h2><?php echo __('Found'); ?></h2>
    <dl>
        <dt><?php echo __('tierart'); ?></dt>
        <dd>
            <?php echo h($found['Found']['Tierart']); ?>&nbsp;
        </dd>
        <dt><?php echo __('Rasse'); ?></dt>
        <dd>
            <?php echo h($found['Found']['Rasse']); ?>
            &nbsp
        </dd>
        <dt><?php echo __('Farbe'); ?></dt>
        <dd>
            <?php echo h($found['Found']['Farbe']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Groesse Cm'); ?></dt>
        <dd>
            <?php echo h($found['Found']['groesse_cm']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Merkmale'); ?></dt>
        <dd>
            <?php echo h($found['Found']['merkmale']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Freitext'); ?></dt>
        <dd>
            <?php echo h($found['Found']['freitext']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Bild');?></dt>
        <dd>
            <?php echo h($found['Found']['bild']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Ort Gefunden');?></dt>
        <dd>
            <?php echo h($found['Found']['ort_gefunden']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Ort Aktuell');?></dt>
        <dd>
            <?php echo h($found['Found']['ort_aktuell']); ?>
            &nbsp;
        </dd>

    </dl>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Edit Found'), array('action' => 'edit', $found['Found']['id'])); ?> </li>
        <li><?php echo $this->Form->postLink(__('Delete Found'), array('action' => 'delete', $found['Found']['id']), null, __('Are you sure you want to delete # %s?', $found['Found']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__('List Founds'), array('action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List my Founds'), array('action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Found'), array('action' => 'add')); ?> </li>
    </ul>
</div>
<?php echo $this->Form->create('Picture',array('type'=>'file', 'action'=>'add' ,'url'=>'/founds/view/'.$found['Found']['id']));
echo $this->Form->input('file', array('type'=>'file'));
echo $this->Form->end('Hochladen');

echo $this->Html->image($pic);



?>