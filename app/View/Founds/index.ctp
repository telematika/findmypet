<!-- File: /app/View/Founds/index.ctp -->




<div class="founds index">
    <h2><?php echo __('Founds'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('name'); ?></th>
            <th><?php echo $this->Paginator->sort('Tierart'); ?></th>
            <th><?php echo $this->Paginator->sort('Rasse'); ?></th>
            <th><?php echo $this->Paginator->sort('Farbe'); ?></th>
            <th><?php echo $this->Paginator->sort('groesse_cm'); ?></th>
            <th><?php echo $this->Paginator->sort('merkmale'); ?></th>
            <th><?php echo $this->Paginator->sort('freitext'); ?></th>
            <th><?php echo $this->Paginator->sort('bild'); ?></th>
            <th><?php echo $this->Paginator->sort('ort_gefunden'); ?></th>
            <th><?php echo $this->Paginator->sort('ort_aktuell'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        <?php
        foreach ($founds as $found): ?>
            <tr>

                <td><?php echo $this->Html->link($found['Found']['name'],
                        array('controller' => 'founds', 'action' => 'view', $found['Found']['id'])); ?></td>
                <td><?php echo h($found['Found']['Tierart']); ?>&nbsp;</td>
                <td><?php echo h($found['Found']['Rasse']); ?>&nbsp;</td>
                <td><?php echo h($found['Found']['Farbe']); ?>&nbsp;</td>
                <td><?php echo h($found['Found']['groesse_cm']); ?>&nbsp;</td>
                <td><?php echo h($found['Found']['merkmale']); ?>&nbsp;</td>
                <td><?php echo h($found['Found']['freitext']); ?>&nbsp;</td>
                <td><?php echo h($found['Found']['bild']); ?>&nbsp;</td>
                <td><?php echo h($found['Found']['ort_gefunden']); ?>&nbsp;</td>
                <td><?php echo h($found['Found']['ort_aktuell']); ?>&nbsp;</td>
                <td class="actions">
                    <?php
                    if ($this->Session->read('Auth.User.id') ==  $found['Found']['id']){
                        echo $this->Html->link(__('View'), array('action' => 'view', $found['Found']['id']));
                        echo $this->Html->link(__('Edit'), array('action' => 'edit', $found['Found']['id']));
                        echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $found['Found']['id']), null, __('Are you sure you want to delete # %s?', $found['Found']['id']));
                    }
                    else{
                    }
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>


    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('New Found'), array('action' => 'add')); ?></li>
    </ul>
</div>

