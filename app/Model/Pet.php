<?php
App::uses('AppModel', 'Model');
/**
 * Pet Model
 *
 */
class Pet extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'id';

    public $belongsTo = 'User';

    public $hasMany = array(
        'Lost' => array(
            'className' => 'Lost')
    );
   }
