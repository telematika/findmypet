<?php
App::uses('AppModel', 'Model');

/**
 * Lost Model
 *
 */
class Lost extends AppModel {

    public $primaryKey = 'id';

    public $belongsTo = 'Pet';
}
/**
 * Created by PhpStorm.
 * User: LehmannH
 * Date: 25.03.14
 * Time: 10:38
 */ 