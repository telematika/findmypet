<?php

App::uses('SimplePasswordHasher', 'Controller/Component/Auth', 'AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel {
    
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Es wird ein Benutzername benötigt'
            )
        ),
        'password' =>array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Es wird ein Passwort benötigt'
            )
        ),
        'role' => array(
            'valid' => array(
                'rule' => array('inList', array('admin', 'author')),
                'message' => 'Bitte eine gültige Funktion / Rolle angeben',
                'allowEmpty' => false
            )
        )
   );

    public $hasMany = array(
        'Pet' => array(
            'className' => 'Pet'
        )
    );
    
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])){
            $passwordHasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password']=$passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }
}
